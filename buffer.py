"""
class for generating frames
"""
from config import *
from blocks import *
from random import randint
from colorama import Fore, Back, Style

class bffr(object):
	def __init__(self,no):
		self.no = no
		self.mat = []
		for i in range(36):
			self.mat.append([])
			for j in range(76):
				self.mat[i].append(' ')

		a = retCldMat()
		"""
		we init every frame with clouds
		"""
		for i in range(len(a)):
			for j in range(len(a[0])):
				self.mat[i][j] = a[i][j]
		"""
		adding ground bricks to every frame
		"""
		for i in range(30, 36, 3):
			for j in range(0, 76, 4):
				a = grndBlcks(i,j).retMat()
				for k in range(len(a)):
					for l in range(len(a[0])):
						self.mat[i+k][j+l] = Fore.RED + a[k][l] + Fore.RESET

		#random scenery generating testing#
		"""
		if (self.no > 0):

			for i in range(randint(3,5)):
				b = airBlcks(2*(randint(4,7)), 4*randint(3, 17))
				a = b.retBlckMat()

				for k in range(len(a)):
					for l in range(len(a[0])):
						self.mat[b.retBlckPos()[0]+k][b.retBlckPos()[1]+l] = a[k][l]
			
		"""

	def updtAtXYBlcks(self, blckObj):
		"""
		this fucntion updates a block object according to it's x,y co-ordinates
		"""
		a = []
		a = blckObj.retMat()
		(x,y) = blckObj.retPos()

		for i in range(len(a)):
			for j in range(len(a[0])):
				if blckObj.color == "yellow":
					self.mat[x+i][y+j] = Fore.YELLOW + a[i][j] + Fore.RESET
				elif blckObj.color == "red":
					self.mat[x+i][y+j] = Fore.RED + a[i][j] + Fore.RESET
				elif blckObj.color == "green":
					self.mat[x+i][y+j] = Fore.GREEN + a[i][j] + Fore.RESET
				else:
					self.mat[x+i][y+j] = a[i][j]

	def retString(self):
		"""
		returns the matrix as a string
		"""
		stringMat = ""
		for i in self.mat:
			for j in i:
				stringMat = stringMat + j
			stringMat = stringMat + '\n'

		return stringMat

	def retMat(self):
		return self.mat

	

	def blit(self, mar, curFrame=0):
		"""
		Function to place mario on to the screen
		"""
		a = mar.retMat()
		b = mar.retPos()
		for i in range(len(a)):
			for j in range(len(a[0])):
				self.mat[b[0]+i][b[1]-curFrame+j] = a[i][j]

	def updFromBoard(self, board, mario):
		"""
		updates from big map based on marios position
		takes that section and places it on frame that we print
		"""

		a = board.retMat()
		b = self.retMat()
		for i in range(len(b)):
			for j in range(len(b[0])):
				self.mat[i][j] = a[i][mario.retDist()-35 + j]
		mario.dist = mario.dist + 1
		return mario.retDist()-35

	def updFromBoardY(self, board, mar):
		"""
		updates based on curFrame
		"""
		a = board.retMat()
		b = self.retMat()
		for i in range(len(b)):
			for j in range(len(b[0])):
				self.mat[i][j] = a[i][mar + j]
		

	def clrMar(self, mar,curFrame=0):
		"""
		clears mario from frame
		"""
		b = mar.retPos()
		(mlen, mwid) = mar.retDim()
		for i in range(mlen):
			for j in range(mwid):
				self.mat[b[0]+i][b[1]+j-curFrame] = ' '

	def blit_enem_frame(self,  enem_objs, curFrame):
		"""
		put current enemies onto frame
		"""
		for enem_obj in enem_objs:
			a = enem_obj.retMat()
			(x,y) = enem_obj.retPos()
			(lent,wid) = (len(a),len(a[0]))

			for i in range(lent):
				for j in range(wid):
					self.mat[x+i][y-curFrame+j] = a[i][j]

	


class board(bffr):                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
	def __init__(self):
		bffr.__init__(self,-1)

	def appndBrd(self,obj):
		"""
		argument is buffer object
		adds frames to make a big map
		"""
		a = obj.retMat()
		for i in range(len(a)):
			self.mat[i].extend(a[i])

	def init_map(self, map):
		"""
		initialises the map
		"""
		for i in range(len(map)-1):
			l =  bffr(-1)
			for j in map[i]:
				l.updtAtXYBlcks(j)
			self.appndBrd(l)