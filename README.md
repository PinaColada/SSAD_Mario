#Super Ascii Bros _(terminal based game)_

The game was implemented as a project for the SSAD course.
The game is a simulation of the first level along with the dragon from 1-4 level.

Mario is implemented as an object that moves up, left, and right. The map is pre-coded and generated on run-time. It's stored in a massive matrix that is referenced to a frame object which is printed as the screen that we see.

Enemies, Mario inherit from the person class, and the different blocks inherit from the block class.

The boss enemy randomly jumps and shoots only when mario is in his vertical range.

The OOPs concepts used are inheritance, data binding, encapsulation and function overloading.

Sound and color have also been implemented as bonus


Directory Structure : 

├── blocks.py
├── buffer.py
├── config.py
├── controllers.py
├── enemies
├── enemy.py
├── input.py
├── main.py
├── mario.py
├── person.py
├── README.md
├── requirements.txt
└── resources
    ├── coin.wav
    ├── fireball.wav
    ├── gameover.wav
    ├── jump.wav
    ├── main_theme.wav
    ├── mariodie.wav
    ├── screaming.wav
    └── stomp.wav


