from blocks import *
from enemy import *
from buffer import *
from os import system
import time

def retCldMat():
    cl1 = ["                                    _ _                  ___                ",
           "                                  _////\_              _////\               ",
           "         _ _ _ _               __////////\           _/////////\_           ",
           "        ////////\_           _////////////\_        /////////////\_ _       ",
           "       |//////////\_        ////////////////\      |/////////////////\      ",
           "        \///////////|       \////////////////       \/////////////////      ",
           "         \//////////                                 \///////////////       "]
    clouds = []

    for i in range(len(cl1)):
        clouds.append([])
        for j in range(len(cl1[0])):
            clouds[i].append(cl1[i][j])
    return clouds


#defining map   
map = []
 #empty buffer for first screen
snap = []
snap.append(airBlcks(3*6, 4*7))
snap.append(airBlcks(3*6, 4*9))
snap.append(airBlcks(3*4, 4*8))
snap.append(coins(3*5,4*8))
snap.append(coinBlcks(3*6, 4*8))
snap.append(pipe(3*8, 4*16 -2))

map.append(snap)

snap = []
snap.append(airBlcks(3*6, 4*6))
snap.append(coins(3*5,4*7))
snap.append(coinBlcks(3*6,4*7))
snap.append(airBlcks(3*6,4*8))
snap.append(airBlcks(3*6,4*9))
snap.append(coinBlcks(3*6,4*10))
snap.append(coins(3*5,4*10))
snap.append(airBlcks(3*6,4*11))
snap.append(pipe(3*8, 4*16-2))

map.append(snap)

snap = []

snap.append(pipe(3*8, 4*16-2))



map.append(snap)

snap = []

for i in range(2):
    for j in range(3):
        snap.append(emptyBlcks(30+i*3,4*6+j*4))


snap.append(airBlcks(3*6, 4*13))
snap.append(airBlcks(3*6, 4*14))
snap.append(airBlcks(3*6, 4*15))

map.append(snap)

snap = []

snap.append(coinBlcks(3*6,4*4))
snap.append(coins(3*5,4*4))
snap.append(coinBlcks(3*6,4*8))
snap.append(coins(3*5,4*8))
snap.append(coinBlcks(3*6,4*12))
snap.append(coins(3*5,4*12))
snap.append(coinBlcks(3*4,4*8))
snap.append(coins(3*3,4*8))
snap.append(pipe(3*8 ,4*15+2))

map.append(snap)

snap = []
snap.append(airBlcks(3*6, 3*4))
snap.append(airBlcks(3*6, 3*6))
snap.append(coinBlcks(3*6, 3*5))
snap.append(coins(3*5, 3*5))

for i in range(7):
    for j in range(7-i-1, 7):
        snap.append(airBlcks( 5*3 + 2*i + 1, 4*12 + 4*j))

map.append(snap)

snap = []

for i in range(2):
    for j in range(3):
        snap.append(emptyBlcks( 30 + i*3, j*4 ))

for i in range(7):
    for j in range(i+1):
        snap.append(airBlcks(3*5 + i*2 + 1, 4*j+ 4*3))

snap.append(pipe(3*8,12*4 + 2))

map.append(snap)

snap = []

snap.append(airBlcks(3*6,4*2))
snap.append(airBlcks(3*6,4*4))
snap.append(airBlcks(3*6,4*5))
snap.append(airBlcks(3*6,4*7))
snap.append(coinBlcks(3*6,4*3))
snap.append(coins(3*5,4*3))
snap.append(coinBlcks(3*6,4*6))
snap.append(coins(3*5,4*6))

for i in range(8):
    for j in range(8-i-1, 8):
        snap.append(airBlcks( 5*3 + 2*i - 1, 4*11 + 4*j))

map.append(snap)



snap = []
snap.append(message(0,0))
map.append(snap)

snap = []
map.append(snap)

for i in range (10, 12):
    for j in range(7, 16):
        snap.append(emptyBlcks(3*i, 4*j))

for i in range ( 16, 18):
    for j in range(7, 16):
        snap.append(lavaBlcks(2*i,4*j))

for j in range(7, 16):
    snap.append(bridge(30, 4*j))

map.append(snap)



enemies = []
enemies.append(mushroom(28, 100))
enemies.append(mushroom(28, 124))
enemies.append(turtle(28, 168))
enemies.append(mushroom(28, 196))
enemies.append(turtle(28, 246))
enemies.append(mushroom(28, 275))
enemies.append(turtle(28, 368))
enemies.append(mushroom(28, 396))
enemies.append(turtle(28, 475))
enemies.append(mushroom(28, 612))
enemies.append(mushroom(28, 635))
enemies.append(bowser(24, 812))

end_game = (27, 827)

def retBowser(enemies):
  for i in enemies:
    if i.retBoss()==True:
      return i

def upd_current_enems(curFrame, enems, e_timer, obj):
  """
  This function checks for enemies in current frame
  """
  enemList = []
  for i in enems:
    if i.retPos()[1] > curFrame and i.retPos()[1] < (curFrame + 69) :
        if i.bossType != True:
            i.move(1, e_timer, None, None)
        else :
            i.move(1, e_timer, obj, enems)

        enemList.append(i)
    if i.retPos()[1] > curFrame + 72 :
        break
  return enemList

def end_animation(frame, obj,curFrame):
  for i in range(16, 7, -1):
    os.system('clear')
    frame.updtAtXYBlcks(emptyBlcks(30, 4*i))
    print(frame.retString())
    time.sleep(0.1)
  time.sleep(0.8)
  tim = 0


  while(obj.retPos()[0] < 27):
    frame.clrMar(obj,curFrame)
    obj.gravity(tim)
    tim = tim + 1
    frame.blit(obj,curFrame)
    print(frame.retString())
    time.sleep(0.1)
    os.system('clear')

  print("You win yay!")
  time.sleep(4)










