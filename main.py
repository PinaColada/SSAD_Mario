from buffer import *
from person import *
from blocks import *
from mario  import *
import os
import sys
import time
from input import Get, input_to
from controllers import *

"""
initialising game
"""

GameOver = False
brd = board()
mar = mario()
frame = bffr(-1)
frame.blit(mar)
lent = len(map)-1

brd.init_map(map)

os.system('clear')
print(frame.retString())

getch = Get()

j_timer = 0
g_timer = 0
e_timer = 0
OnGround = True	 	
JumpInProgress = False
curFrame = 0
lives = 3
coins = 0
enems_killed = 0

os.system('aplay resources/main_theme.wav&')

while(GameOver != True):
		

	input = input_to(getch)
	os.system('clear')
	if marioCollideEnemy(mar, frame) == 1 or mar.retPos()[0]>30:
		"""
		if mario dies from enemy or falling 
		"""
		mar.updDist(0)
		mar.updY(0)
		curFrame = 0
		lives = lives - 1 
		if(lives == 0):
			GameOver = True
		
		os.system('aplay -q resources/mariodie.wav&')
		continue
		


	if marioEnemyKill(mar, frame) == 1:
		"""
		if mario jumps on the enemy
		"""
		enems_killed = enems_killed + 1
		os.system('aplay -q resources/stomp.wav&')
		mar.enemies_kill(enemies, curFrame)


	print(frame.retString()+"\n"+"Score : "+str(curFrame+35 + 4*enems_killed) + "\t"+ "Lives : " + str(lives) + "\t" + "Coins : " + str(coins) + "\t" )
	if(GameOver):
		time.sleep(2)
	frame.updFromBoardY(brd, curFrame)

	
	if input == 'q':
		GameOver = True

	if marioCollideSideCoin(mar, frame) == 1:
		coins = coins + 1
		os.system('aplay -q resources/coin.wav&')
		brd.updtAtXYBlcks(emptyBlcks(mar.retPos()[0],mar.retDist()+mar.retDim()[1]))

	if marioCollideSideCoin(mar, frame) == 2:
		os.system('aplay -q resources/coin.wav&')
		brd.updtAtXYBlcks(emptyBlcks(mar.retPos()[0],mar.retDist()-3))	


	if input == 'd' and marioCollideSide(mar, frame)!= 1 :
		if(mar.moveRight(1,lent)==1):
			pass
		else:
			curFrame = frame.updFromBoard(brd, mar)
			

	elif input == 'a' and marioCollideSideL(mar, frame)!= 1 :
		frame.clrMar(mar)
		mar.moveLeft(1)
		
			
	if ( marioCollideUp(mar, frame) == 1 ):
		JumpInProgress = False
	
	
	if ( (JumpInProgress!=True)
	 and (marioCollide(mar,frame)!=1) ) :

		frame.clrMar(mar)
		mar.gravity(g_timer)
		
		g_timer = g_timer + 1
		if marioCollide(mar, frame) == 1 : 
			OnGround = True
			g_timer = 0

	if (input == 'w'):
		os.system('aplay -q resources/jump.wav&')
		JumpInProgress = True
	if (JumpInProgress):
		
		JumpInProgress = True
		OnGround = False
		frame.clrMar(mar)
		mar.jump(j_timer)
		
		j_timer = j_timer + 1
		if(j_timer==18):
			JumpInProgress = False
		j_timer = j_timer % 18


	frame.blit_enem_frame(upd_current_enems(curFrame, enemies, e_timer, mar),curFrame)
	e_timer = e_timer + 1
	e_timer = e_timer % 16
	frame.blit(mar)

	if (mar.retPos()[0],mar.retDist()) == end_game :
		end_animation(frame,retBowser(enemies), curFrame)
		GameOver = True



	time.sleep(0.01)

os.system('clear')
os.system("ps axf | grep aplay | grep -v grep | awk \'{print \"kill -9 \" $1}\' | sh 2> /dev/null" )