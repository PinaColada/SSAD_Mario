class blocks(object):
	"""
	Class for basic building blocks of the map
	"""

	def __init__(self, dest, typ, x, y):
		self.dest = dest
		self.mat  = []
		self.type = typ
		self.x = x
		self.y = y
		self.color = None

	def retMat(self):
		return self.mat

	def retPos(self):
		return (self.x,self.y)

class grndBlcks(blocks):
	def __init__(self, x, y):
		blocks.__init__(self, False, 1, x, y)
		self.color = "yellow"
		self.mat = [
			['W','W','W','|'],
			['*','*','*','|'],
			['M','M','M','|']
		]

class airBlcks(blocks):
	def __init__(self, x, y):
		blocks.__init__(self, False, 2, x, y)
		self.color = "red"
		self.mat = [
		['|','*','*','|'],
		['|','*','*','|']
		]

class coinBlcks(blocks) :
	def __init__(self, x, y):
		blocks.__init__(self, True, 3, x, y)
		self.color = "yellow"
		self.mat = [
		['|','?','?','|'],
		['|','?','?','|']
		]
class pipe(blocks):
	def __init__(self, x, y):
		blocks.__init__(self, False, 4, x, y)
		newMat = []
		newMat.append([])
		self.color = "green"
		for i in range(12):
			newMat[0].append('W')
		"""
		this for loop is for building the bottom part of the block
		"""
		for i in range(1,6):
			newMat.append([])
			newMat[i].extend([' ',' ','|','/','|','/','/','/','/','|',' ',' '])

		self.mat = newMat

class emptyBlcks(blocks):
	"""
	When we want to remove something from the screen,
	we blit this block

	"""
	def __init__(self, x, y):
		self.color = "blue"
		blocks.__init__(self, False, 1, x, y)
		self.mat = [
			[' ',' ',' ',' '],
			[' ',' ',' ',' '],
			[' ',' ',' ',' ']
		]
		

class lavaBlcks(blocks):
	def __init__(self, x, y):
		blocks.__init__(self, False, 1, x, y)
		self.color = "red"
		self.mat = [
		["^",".","^","."], 
		[".","^",".","^"]
		]
		
class bridge(blocks):
	def __init__(self, x, y):
		blocks.__init__(self, False, 1, x, y)
		self.color = "black"
		self.mat = [
		["|","|","|","|"], 
		["^","*","^","*"]
		]

class coins(blocks):
	def __init__(self, x, y):
		blocks.__init__(self, False, 1, x, y)
		
		self.mat = [
		[" ","$","$"," "], 
		[" ","$","$"," "]
		]
class message(blocks):
	def __init__(self, x, y):
		blocks.__init__(self, False, 1, x, y)
		a = ["_____________",
		"|WELCOME TO |",
		"|BOSS LEVEL!|",
		"|BEWARE!    |",
		"-------------"
		]
		"""
		converting the string to a matrix
		"""

		for i in range(len(a)):
			self.mat.append([])
			for j in range(len(a[0])):
				self.mat[i].append(a[i][j])
		





