
def marioCollide(mar, frame) :
	# collision with floor returns 1
	(x,y) = mar.retPos()
	(lent, wid) = mar.retDim()
	a = frame.retMat()
		
	for i in range(wid):
		if a[x+lent][y+i] != ' ' :
			return 1

def marioCollideSide(mar, frame) :
	"""
	checking right collision
	"""
	(x,y) = mar.retPos()
	(lent, wid) = mar.retDim()
	a = frame.retMat()

	
	for i in range(lent):
		if a[x+i][y+wid] != ' ' :
			return 1

def marioCollideSideL(mar, frame) :
	"""
	checking left collision
	"""
	(x,y) = mar.retPos()
	(lent, wid) = mar.retDim()
	a = frame.retMat()

	
	for i in range(lent):
		if a[x+i][y-1] != ' ' :
			return 1

def marioCollideUp(mar, frame) :
	"""
	checking upwards collision
	"""
	(x,y) = mar.retPos()
	(lent, wid) = mar.retDim()
	a = frame.retMat()

	
	for i in range(wid):
		if a[x-1][y+i] != ' ' :
			return 1

def marioCollideEnemy(mar, frame):
	"""
	Collision to check if mario is killed by enemy
	"""
	(x,y) = mar.retPos()
	(lent, wid) = mar.retDim()
	a = frame.retMat()

	for i in range(lent):
		if a[x+i][y+wid] in ['@','(','<']:
			return 1

def marioEnemyKill(mar, frame) :
	"""
	checks if mario lands on enemy
	"""
	(x, y) = mar.retPos()
	(lent, wid) = mar.retDim()
	a = frame.retMat()

	for i in range(wid):
		if a[x+lent][y+i] in ['^','@','(',')','{','}'] :
			return 1

def marioCollideSideCoin(mar, frame) :
	"""
	coin collection checker
	"""
	(x,y) = mar.retPos()
	(lent, wid) = mar.retDim()
	a = frame.retMat()

	
	for i in range(lent):
		if a[x+i][y+wid] == '$':
			return 1
		if a[x+i][y-1] == '$' :
			return 2


