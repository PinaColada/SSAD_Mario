"""
Class for basic character model
"""

class person(object):
	def __init__(self, x, y, lent, wid):
		self.x   = x
		self.y   = y
		self.len = lent
		self.wid = wid
		self.mat = None
	
	def retPos(self):
		return (self.x,self.y)
	
	def retDim(self):
		return(self.len, self.wid)

	def updX(self, x):
		self.x = x
		return

	def updY(self, y):
		self.y = y
		return

	def retMat(self):
		return self.mat

	def moveLeft(self, x):
		self.y = self.y - x

	def moveRight(self, x):
		self.y = self.y + x

	def gravity(self, g_timer):
		"""
		the effect of gravity increases with time.
		I've implemented that using modulo on the timer
		"""
		if g_timer < 6 :
			if g_timer % 6 == 0 :
				self.updX(self.x + 1)

		elif g_timer < 12 :
			if g_timer % 3 == 0 :
				self.updX(self.x + 1)

		elif g_timer < 18 :
			if g_timer % 1 == 0 :
				self.updX(self.x + 1)

		elif g_timer < 24 : 
			self.updX(self.x + 1)

		elif g_timer > 24 :
			self.updX(self.x + 1)

	def jump(self, j_timer):
		"""
		Similar concept to gravity
		"""
		
		if j_timer < 6 :
			if j_timer % 1 == 0 :
				self.updX( self.x - 2)

		elif j_timer < 12 :
			if j_timer % 3 == 0 :
				self.updX(self.x - 1)

		elif j_timer < 18 :
			if j_timer % 6 == 0 :
				self.updX(self.x - 1)