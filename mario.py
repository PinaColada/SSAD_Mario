"""
class for mario
"""
from colorama import Back
from person import *

class mario(person):

    def __init__(self):
        self.dist = 0
        person.__init__(self, 27, 0, 3, 3)
        self.mat = [
             [' ',Back.RED+'@'+Back.RESET,' '],
             ['/','V','\\'],
             [' ','L','L']
            

        ]

    def retDist(self):
        return self.dist
    def updDist(self, dist):
        self.dist = dist


    def enemies_kill(self, enems, curFrame):
        """
        if mario kills the enemy
        enemy is removed from the list
        """
        for i in enems:
            if ((i.retPos()[1] < self.retDist() + 2) and (i.retPos()[1] > self.retDist() - 2)):
                enems.remove(i)
            if i.retPos()[1] > curFrame + 76 :
                break


    def moveRight(self, x, lent):
        """
        returns 1 if player moves
        returns 2 if screen moves
            """
        
        if((self.y < 36) or (self.dist > ((lent) * 76 + 30))):
            self.y = self.y + x
            self.dist = self.dist + x
            return 1
        else:
            return 2

    def moveLeft(self, x):
        if(self.y > 0):
            self.y = self.y - x
            self.dist = self.dist - x

    